package top.lingkang.finalsecurity.annotation.impl;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import top.lingkang.finalsecurity.common.constants.FinalConstants;
import top.lingkang.finalsecurity.common.error.FinalNotLoginException;
import top.lingkang.finalsecurity.config.FinalSecurityConfiguration;
import top.lingkang.finalsecurity.http.FinalRequestContext;
import top.lingkang.finalsecurity.http.FinalSecurityHolder;

/**
 * @author lingkang
 * Created by 2022/1/11
 * @since 1.0.0
 */
@Aspect
public class FinalCheckLoginAnnotation {
    @Autowired(required = false)
    private FinalSecurityHolder securityHolder;
    @Autowired(required = false)
    private FinalSecurityConfiguration finalSecurityConfiguration;

    @Around("@within(top.lingkang.finalsecurity.common.annotation.FinalCheckLogin) || @annotation(top.lingkang.finalsecurity.common.annotation.FinalCheckLogin)")
    public Object before(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!finalSecurityConfiguration.getProperties().getCheckPathCache().getExcludePath().contains(FinalRequestContext.getRequest().getServletPath())) {
            if (!securityHolder.isLogin()) {
                throw new FinalNotLoginException(FinalConstants.NOT_LOGIN_MSG);
            }
        }
        return joinPoint.proceed();
    }
}
