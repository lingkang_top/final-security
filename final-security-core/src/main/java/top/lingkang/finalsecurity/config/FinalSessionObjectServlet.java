package top.lingkang.finalsecurity.config;

import top.lingkang.finalsecurity.common.base.FinalSessionObject;

import javax.servlet.http.HttpSession;

/**
 * @author lingkang
 * Created by 2022/10/28
 * @since 3.0.0
 */
class FinalSessionObjectServlet implements FinalSessionObject {

    private HttpSession session;

    public FinalSessionObjectServlet(HttpSession session) {
        this.session = session;
    }

    @Override
    public Object getAttribute(String name) {
        return session.getAttribute(name);
    }
}
