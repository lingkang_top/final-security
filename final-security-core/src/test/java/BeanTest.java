import top.lingkang.finalsecurity.common.error.FinalBaseException;
import top.lingkang.finalsecurity.common.error.FinalNotLoginException;

/**
 * @author lingkang
 * Created by 2022/1/10
 */
public class BeanTest {
    public static void main(String[] args) {
        FinalBaseException base=new FinalBaseException("123");
        FinalNotLoginException exception=new FinalNotLoginException("123");
        System.out.println(base.getClass().isAssignableFrom(exception.getClass()));
        System.out.println(exception.getClass().isAssignableFrom(base.getClass()));

        System.out.println(base.getClass().isAssignableFrom(FinalBaseException.class));
    }
}
