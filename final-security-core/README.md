# 使用于JAVAEE9之前

适用 springboot 3.x以下，及其传统javaEE9之前的版本
```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-security-core-jakarta</artifactId>
    <version>3.1.0</version><!-- 以最新发布版本为准 -->
</dependency>
```

`Maven`库查看最新版本: [https://repo1.maven.org/maven2/top/lingkang/final-security-core](https://repo1.maven.org/maven2/top/lingkang/final-security-core)
