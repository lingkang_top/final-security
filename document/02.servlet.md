传统 servlet 中

引入依赖

```pom
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-security-core</artifactId>
    <version>3.x.x</version><!-- 以最新发布版本为准 -->
</dependency>
```

配置 `web.xml`

```pom
<filter-mapping>
    <filter-name>securityFilter</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
<filter>
    <filter-name>securityFilter</filter-name>
    <filter-class>top.lingkang.example_servlet.FinalSecurityConfig</filter-class>
</filter>
```

配置类

```java
public class FinalSecurityConfig extends FinalSecurityConfiguration {
    @Override
    protected void config(FinalHttpProperties properties) {
        // 对项目进行配置
        properties.checkAuthorize()
                .pathMatchers("/user").hasAnyRole("user", "vip1") // 有其中任意角色就能访问
                .pathMatchers("/vip/**").hasAllRole("user", "vip1") // 必须有所有角色才能访问
                .pathMatchers("/about").hasLogin();// 需要登录才能访问
    }
}
```

代码中使用

```java
// 实例化一次，全局调用
public static FinalSecurityHolder securityHolder=new FinalSecurityHolder();
public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    // 直接使用 FinalSecurityHolder 后面不再赘述
    securityHolder.login("zhangsan",null);
    // ...
}
```