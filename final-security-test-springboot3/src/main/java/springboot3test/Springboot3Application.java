package springboot3test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@SpringBootApplication
public class Springboot3Application {
    public static void main(String[] args) {
        SpringApplication.run(Springboot3Application.class, args);
    }
}
