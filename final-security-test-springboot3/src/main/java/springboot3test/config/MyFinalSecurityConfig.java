package springboot3test.config;

import org.springframework.stereotype.Component;
import top.lingkang.finalsecurity.common.base.FinalHttpProperties;
import top.lingkang.finalsecurity.jakarta.FinalSecurityConfiguration;
import top.lingkang.finalsecurity.jakarta.annotation.EnableFinalSecurityAnnotation;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@EnableFinalSecurityAnnotation // 开启 FinalSecurity AOP鉴权注解
@Component
public class MyFinalSecurityConfig extends FinalSecurityConfiguration {
    @Override
    protected void config(FinalHttpProperties properties) {
        properties.setExcludePath("/api/login","/api/logout");
        properties.checkAuthorize()
                .pathMatchers("/api/**").hasLogin();

    }
}
