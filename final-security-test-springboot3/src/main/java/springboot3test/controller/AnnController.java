package springboot3test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.lingkang.finalsecurity.common.annotation.FinalCheck;
import top.lingkang.finalsecurity.common.annotation.FinalCheckLogin;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@RestController
@RequestMapping("/ann")
public class AnnController {
    @GetMapping("not")
    public Object not() {
        return "ok";
    }

    @FinalCheckLogin
    @GetMapping("login")
    public Object login() {
        return "ok";
    }

    @FinalCheck(anyRole = "user")
    @GetMapping("role")
    public Object role() {
        return "ok";
    }
}
