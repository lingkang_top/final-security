package springboot3test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.lingkang.finalsecurity.jakarta.http.FinalSecurityHolder;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@RestController
@RequestMapping("/api")
public class WebController {
    @Autowired
    private FinalSecurityHolder finalSecurityHolder;

    @GetMapping("/")
    public Object index() {
        return "ok";
    }

    @GetMapping("/login")
    public Object login() {
        finalSecurityHolder.login("admin", new String[0]);
        return "ok";
    }

    @GetMapping("/logout")
    public Object logout() {
        finalSecurityHolder.logout();
        return "ok";
    }

    @GetMapping("/role")
    public Object role() {
        return finalSecurityHolder.getRole();
    }
}
