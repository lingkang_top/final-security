package solon.conftroller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import top.lingkang.finalsecurity.common.annotation.FinalCheck;
import top.lingkang.finalsecurity.common.annotation.FinalCheckLogin;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@Controller
@Mapping("/ann")
public class AnnController {
    @Mapping("not")
    public Object not() {
        return "ok";
    }

    @FinalCheckLogin
    @Mapping("login")
    public Object login() {
        return "ok";
    }

    @FinalCheck(anyRole = "user")
    @Mapping("role")
    public Object role() {
        return "ok";
    }
}
