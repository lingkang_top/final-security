package solon.conftroller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import top.lingkang.finalsecurity.solonplugin.FinalSecurityHolder;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@Controller
@Mapping("/api")
public class WebController {
    @Inject
    private FinalSecurityHolder finalSecurityHolder;

    @Mapping("/")
    public Object index() {
        return "ok";
    }

    @Mapping("/login")
    public Object login() {
        finalSecurityHolder.login("admin", new String[0]);
        return "ok";
    }

    @Mapping("/logout")
    public Object logout() {
        finalSecurityHolder.logout();
        return "ok";
    }

    @Mapping("/role")
    public Object role() {
        return finalSecurityHolder.getRole();
    }
}
