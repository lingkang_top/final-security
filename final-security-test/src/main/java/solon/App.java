package solon;

import org.noear.solon.Solon;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
public class App {
    public static void main(String[] args) {
        Solon.start(App.class,args);
    }
}
