package solon.config;

import org.noear.solon.annotation.Component;
import top.lingkang.finalsecurity.common.base.FinalHttpProperties;
import top.lingkang.finalsecurity.solonplugin.config.FinalSecurityConfiguration;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@Component
public class FinalSecurityConfig extends FinalSecurityConfiguration {
    @Override
    protected void config(FinalHttpProperties config) {
        config.setExcludePath("/api/login","/api/logout");
        config.checkAuthorize()
                .pathMatchers("/api/**").hasLogin();
    }
}
