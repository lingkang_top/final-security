package springboot2.config;

import org.springframework.context.annotation.Configuration;
import top.lingkang.finalsecurity.annotation.EnableFinalSecurityAnnotation;
import top.lingkang.finalsecurity.common.base.FinalHttpProperties;
import top.lingkang.finalsecurity.config.FinalSecurityConfiguration;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@EnableFinalSecurityAnnotation // 开启 FinalSecurity AOP鉴权注解
@Configuration
public class SpringBoot2FinalSecurityConfig extends FinalSecurityConfiguration {
    @Override
    protected void config(FinalHttpProperties config) {
        config.setExcludePath("/api/login","/api/logout");
        config.checkAuthorize()
                .pathMatchers("/api/**").hasLogin();
    }
}
