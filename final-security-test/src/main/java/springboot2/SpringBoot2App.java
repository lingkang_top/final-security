package springboot2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lingkang
 * Created by 2024/3/10
 */
@SpringBootApplication
public class SpringBoot2App {
    public static void main(String[] args) {
        SpringApplication.run(SpringBoot2App.class, args);
    }
}
