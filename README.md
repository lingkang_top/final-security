# final-security

## 介绍

final-security，一个基于RBAC，专注于授权认证的轻量级框架<br/>

## 底层原理

云淡风轻，回归真我，专注其一，`final-security` 底层依赖于`web`的`session`。

### 依赖
`spring` 体系中。
```pom
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<!-- springboot 2.x 版本使用 final-security-core -->
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-security-core</artifactId>
    <version>3.1.0</version><!-- 以最新发布版本为准 -->
</dependency>

<!-- springboot 3.x 以上版本使用 final-security-core-jakarta -->
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-security-core-jakarta</artifactId>
    <version>3.1.0</version><!-- 以最新发布版本为准 -->
</dependency>
```
`Maven`库查看最新版本: [https://repo1.maven.org/maven2/top/lingkang/final-security-core](https://repo1.maven.org/maven2/top/lingkang/final-security-core)


### 1. 配置

```java
@EnableFinalSecurityAnnotation // 开启 FinalSecurity AOP鉴权注解，需要添加apo依赖
@Configuration
public class Myconfig extends FinalSecurityConfiguration {
    @Override
    protected void config(FinalHttpProperties properties) {
        // 对项目进行配置
        properties.checkAuthorize()
                .pathMatchers("/user").hasAnyRole("user", "vip1") // 有其中任意角色就能访问
                .pathMatchers("/vip/**").hasAllRole("user", "vip1");// 必须同时有所有角色才能访问
        
        // 排除鉴权路径匹配, 匹配优先级别：排除路径 > checkAuthorize > 注解
        properties.setExcludePath("/login", "/logout", "/vip/total", "/vip/user/**", "/**.js", "/**.css");
    }
}
```

`更多配置请查看 FinalConfigProperties 类`
* 默认所有请求都能通过。
* 通过指定路径，路径通配符等进行角色权限鉴权。注意，排除路径会使checkAuthorize失效。优先等级：排除路径 > checkAuthorize > 注解



### 2. 登录

自动装配 `FinalSecurityHolder` 持有者进行操作

```java
@Autowired
private FinalSecurityHolder securityHolder;
```

登录`username`通常指用户唯一`username`，化繁为简，在`web`中会生成对应的会话，`final-security`底层基于`session`验证

```java
@Autowired
private FinalSecurityHolder securityHolder;

@GetMapping("login")
public Object login() {
    securityHolder.login("asd", new String[]{"user"});
    securityHolder.getUsername();// 获取会话用户名 string
    securityHolder.getRole(); // 获取会话中的角色 array
    securityHolder.isLogin(); // 判断当前会话是否登录 boolean
    return "login-success";
}
```

> 登录后即可访问其他资源

### 3. 注销

注销当前用户`username`

```java
// 注销当前会话
@GetMapping("logout")
public Object logout() {
    securityHolder.logout();
    return "ok";
}
```


### 3. 前端解析视图中获取用户、角色

直接从`session`中读取即可。在`jsp`中

```html
is login：${sessionScope.final_isLogin}<br/>
username：${sessionScope.final_loginUsername}<br/>
role：<br/>
${sessionScope.final_hasRoles}
<br/>
<%=Arrays.toString((String[]) request.getSession().getAttribute("final_hasRoles"))%>
<br/>
```

![pay](https://gitee.com/lingkang_top/final-security/raw/master/document/fontend-servlet.png)

在`Thymeleaf`中

```html
是否登录了：[[${session.final_isLogin}]]
<br>
登录的用户：[[${session.final_loginUsername}]]
<br>
用户拥有的角色：[[${session.final_hasRoles}]]
<br>
<!-- Thymeleaf 遍历-->
<span th:each="item:${session.final_hasRoles}">[[${item}]]，</span>
```

![pay](https://gitee.com/lingkang_top/final-security/raw/master/document/fontend-springboot.png)

> 所有`session`变量请参考: `FinalSessionKey` 类

### 4. 使用注解进行鉴权

`final-security`没有引入AOP注解所需依赖，需要手动引入AOP依赖，否则将会报AOP包类找不到异常。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

开启注解：

```java
@EnableFinalSecurityAnnotation // 开启 FinalSecurity AOP鉴权注解
@Configuration
public class Myconfig extends FinalSecurityConfiguration {
    // ...
}
```

使用注解作用于`controller`上

```java
    // 检查登录情况
    @FinalCheckLogin
    @GetMapping("/")
    public Object index() {
        return "index";
    }
    
    // 通过角色权限检查
    @FinalCheck(orRole = "admin",andRole = {"admin","system"})
    @GetMapping("/")
    public Object index() {
        return "index";
        }
```

作用于`service`上

```java
@Service
public class UserServiceImpl implements UserService {
    @FinalCheck(orRole = "user")
    @Override
    public String getNickname() {
        return "lingkang";
    }
}
```

作用在类上

```java
@FinalCheck(orRole = "user")
@Service
public class UserServiceImpl implements UserService {

    @Override
    public String getNickname() {
        return "lingkang";
    }

    @FinalCheck(orRole = "admin")
    @Override
    public String getUsername() {
        return "asd";
    }
}
```

### 5. 自定义异常处理

默认配置可能不满足实际场景需求，这里介绍了`final-security`的自定义异常处理功能。
`final-security`的所有异常处理均在接口 `FinalExceptionHandler` ，初始化配置时可设置它进行自定义。

```java
properties.setExcludePath(new String[]{"/login", "/logout"});
// 自定义异常处理
properties.setExceptionHandler(new FinalExceptionHandler() {
    @Override
    public void permissionException(Exception e, HttpServletRequest request, HttpServletResponse response) {
        // 异常处理
    }

    @Override
    public void notLoginException(Exception e, HttpServletRequest request, HttpServletResponse response) {
        // 异常处理
    }
    
    @Override
    public void exception(Exception e, HttpServletRequest request, HttpServletResponse response) {
        // 异常处理
    }
});
```

> 常见的自定义有重定向到登录、未授权响应等。

## solon 中使用

**请查看:** [final-security-solon-plugin](https://gitee.com/lingkang_top/final-security/tree/master/final-security-solon-plugin)

## servlet 中使用

**请查看:** [https://gitee.com/lingkang_top/final-security/blob/master/document/02.servlet.md](https://gitee.com/lingkang_top/final-security/blob/master/document/02.servlet.md)


## 多节点、集群

`final-security`依赖`session`，因此整合分布式会话可以轻松实现无限扩展集群。

[多节点、集群方案](https://gitee.com/lingkang_top/final-security/blob/master/document/03.cluster.md)

## 加密工具类

[加密工具类](https://gitee.com/lingkang_top/final-security/blob/master/document/%E5%8A%A0%E5%AF%86%E7%9B%B8%E5%85%B3.md)

## 其他

有问题issues，也可以邮箱：**ling-kang@qq.com**
<br><br>也能请我喝冰可乐：
<br>
![pay](https://gitee.com/lingkang_top/final-security/raw/master/document/pay.png)
<br><br>