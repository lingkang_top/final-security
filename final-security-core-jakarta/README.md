# 使用于JAVAEE9+

适用 springboot 3.x

```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-security-core-jakarta</artifactId>
    <version>3.1.0</version><!-- 以最新发布版本为准 -->
</dependency>
<!-- AOP 支持-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```
`Maven`库查看最新版本: [https://repo1.maven.org/maven2/top/lingkang/final-security-core](https://repo1.maven.org/maven2/top/lingkang/final-security-core)

# 用法

用法：[https://gitee.com/lingkang_top/final-security](https://gitee.com/lingkang_top/final-security)