package top.lingkang.finalsecurity.jakarta;

import top.lingkang.finalsecurity.common.base.FinalSessionObject;

import jakarta.servlet.http.HttpSession;

/**
 * @author lingkang
 * Created by 2022/10/28
 * @since 3.0.0
 */
class FinalSessionObjectJakarta implements FinalSessionObject {

    private HttpSession session;

    public FinalSessionObjectJakarta(HttpSession session) {
        this.session = session;
    }

    @Override
    public Object getAttribute(String name) {
        return session.getAttribute(name);
    }
}
