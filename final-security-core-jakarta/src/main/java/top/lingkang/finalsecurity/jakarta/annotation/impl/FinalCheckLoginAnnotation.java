package top.lingkang.finalsecurity.jakarta.annotation.impl;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import top.lingkang.finalsecurity.common.constants.FinalConstants;
import top.lingkang.finalsecurity.common.error.FinalPermissionException;
import top.lingkang.finalsecurity.jakarta.FinalSecurityConfiguration;
import top.lingkang.finalsecurity.jakarta.http.FinalRequestContext;
import top.lingkang.finalsecurity.jakarta.http.FinalSecurityHolder;

/**
 * @author lingkang
 * Created by 2022/1/11
 * @since 1.0.0
 */
@Aspect
public class FinalCheckLoginAnnotation {
    @Autowired(required = false)
    private FinalSecurityHolder securityHolder;
    @Autowired(required = false)
    private FinalSecurityConfiguration finalSecurityConfiguration;

    @Around("@within(top.lingkang.finalsecurity.common.annotation.FinalCheckLogin) || @annotation(top.lingkang.finalsecurity.common.annotation.FinalCheckLogin)")
    public Object before(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!finalSecurityConfiguration.getProperties().getCheckPathCache().getExcludePath().contains(FinalRequestContext.getRequest().getServletPath())) {
            if (!securityHolder.isLogin()) {
                throw new FinalPermissionException(FinalConstants.UNAUTHORIZED_MSG);
            }
        }
        return joinPoint.proceed();
    }
}
