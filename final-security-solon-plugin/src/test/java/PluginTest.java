import org.noear.solon.Solon;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;

/**
 * @author lingkang
 * Created by 2022/10/28
 */
@Controller
public class PluginTest {

    public static void main(String[] args) {
        Solon.start(PluginTest.class, new String[]{});
    }

    @Get
    @Mapping("/")
    public Object index() {
        return "123";
    }

}
