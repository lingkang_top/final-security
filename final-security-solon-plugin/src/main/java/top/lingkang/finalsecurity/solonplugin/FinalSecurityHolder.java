package top.lingkang.finalsecurity.solonplugin;

import org.noear.solon.core.handle.Context;
import top.lingkang.finalsecurity.common.base.FinalHolder;
import top.lingkang.finalsecurity.common.constants.FinalSessionKey;

/**
 * @author lingkang
 * Created by 2022/10/28
 * 默认已经注入到容器中
 * @since 3.0.0
 */
public class FinalSecurityHolder extends FinalHolder {
    @Override
    public void login(String username, String[] role) {
        Context current = Context.current();
        current.sessionSet(FinalSessionKey.LOGIN_USERNAME, username);
        current.sessionSet(FinalSessionKey.HAS_ROLE, role);
        current.sessionSet(FinalSessionKey.IS_LOGIN, true);
    }

    @Override
    public void logout() {
        Context current = Context.current();
        current.sessionRemove(FinalSessionKey.IS_LOGIN);
        current.sessionRemove(FinalSessionKey.HAS_ROLE);
        current.sessionRemove(FinalSessionKey.LOGIN_USERNAME);
    }

    @Override
    public String[] getRole() {
        Object finalRole = Context.current().session(FinalSessionKey.HAS_ROLE);
        if (finalRole != null) {
            return (String[]) finalRole;
        }
        return null;
    }

    @Override
    public String getUsername() {
        Object username = Context.current().session(FinalSessionKey.LOGIN_USERNAME);
        if (username != null) {
            return (String) username;
        }
        return null;
    }

    @Override
    public boolean isLogin() {
        Object login = Context.current().session(FinalSessionKey.IS_LOGIN);
        if (login != null)
            return (boolean) login;
        return false;
    }
}
