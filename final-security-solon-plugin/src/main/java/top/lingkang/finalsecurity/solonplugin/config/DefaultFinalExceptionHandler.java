package top.lingkang.finalsecurity.solonplugin.config;

import org.noear.solon.core.handle.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.lingkang.finalsecurity.common.base.FinalExceptionHandler;

/**
 * @author lingkang
 * Created by 2022/10/28
 * @since 3.0.0
 */
public class DefaultFinalExceptionHandler implements FinalExceptionHandler<Context, Context> {
    private static final Logger log = LoggerFactory.getLogger(DefaultFinalExceptionHandler.class);

    /**
     * 需要注意， request、response 在 solon 下均为 org.noear.solon.core.handle.Context
     *
     * @param e
     * @param request  org.noear.solon.core.handle.Context
     * @param response org.noear.solon.core.handle.Context
     */
    @Override
    public void permissionException(Throwable e, Context request, Context response) {
        log.warn("URL=" + request.path() + "  " + e.getMessage());
        response.status(403);
        response.outputAsJson("{\"code\":1,\"msg\":\"" + e.getMessage() + "\"}");
    }

    /**
     * 需要注意， request、response 在 solon 下均为 org.noear.solon.core.handle.Context
     *
     * @param e
     * @param request  org.noear.solon.core.handle.Context
     * @param response org.noear.solon.core.handle.Context
     */
    @Override
    public void notLoginException(Throwable e, Context request, Context response) {
        log.warn("URL=" + request.path() + "  " + e.getMessage());
        response.status(403);
        response.outputAsJson("{\"code\":1,\"msg\":\"" + e.getMessage() + "\"}");
    }

    /**
     * 需要注意， request、response 在 solon 下均为 org.noear.solon.core.handle.Context
     *
     * @param e
     * @param request  org.noear.solon.core.handle.Context
     * @param response org.noear.solon.core.handle.Context
     */
    @Override
    public void exception(Throwable e, Context request, Context response) {
        log.warn("URL=" + request.path() + "  " + e.getMessage());
        response.status(200);
        response.outputAsJson("{\"code\":1,\"msg\":\"" + e.getMessage() + "\"}");
    }
}
