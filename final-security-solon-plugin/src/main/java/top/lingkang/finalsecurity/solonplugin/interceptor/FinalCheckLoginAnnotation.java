package top.lingkang.finalsecurity.solonplugin.interceptor;

import org.noear.solon.Solon;
import org.noear.solon.core.aspect.Interceptor;
import org.noear.solon.core.aspect.Invocation;
import org.noear.solon.core.handle.Context;
import top.lingkang.finalsecurity.common.base.FinalHttpProperties;
import top.lingkang.finalsecurity.common.constants.FinalConstants;
import top.lingkang.finalsecurity.common.error.FinalNotLoginException;
import top.lingkang.finalsecurity.solonplugin.FinalSecurityHolder;

/**
 * @author lingkang
 * Created by 2022/10/28
 * @since 3.0.0
 */
public class FinalCheckLoginAnnotation implements Interceptor {
    public FinalHttpProperties properties;
    private final FinalSecurityHolder securityHolder = Solon.context().getBean(FinalSecurityHolder.class);

    @Override
    public Object doIntercept(Invocation inv) throws Throwable {
        if (properties.getCheckPathCache().getExcludePath().contains(Context.current().path())) {
            return inv.invoke();
        }

        if (!securityHolder.isLogin()) {
            throw new FinalNotLoginException(FinalConstants.NOT_LOGIN_MSG);
        }
        return inv.invoke();
    }
}
