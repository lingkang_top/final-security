package top.lingkang.finalsecurity.solonplugin.config;

import org.noear.solon.core.handle.Context;
import top.lingkang.finalsecurity.common.base.FinalSessionObject;

/**
 * @author lingkang
 * Created by 2022/10/28
 * @since 3.0.0
 */
class FinalSessionObjectSolon implements FinalSessionObject {

    Context context;

    public FinalSessionObjectSolon(Context context) {
        this.context = context;
    }

    @Override
    public Object getAttribute(String name) {
        return context.session(name);
    }
}
