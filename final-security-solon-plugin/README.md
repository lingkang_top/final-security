# final-security-solon-plugin

## 介绍

`final-security`，一个基于`RBAC`，专注于授权认证的轻量级框架<br/>
`final-security-solon-plugin` 是`final-security`的`solon`实现
## 返璞归真

云淡风轻，回归真我，专注其一，`final-security` 浴火重生3.x，底层大变动。

# 01.快速入门
```pom
<dependency>
    <groupId>org.noear</groupId>
    <artifactId>solon-web</artifactId>
    <version>${solon.version}</version>
</dependency>

<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-security-solon-plugin</artifactId>
    <version>3.x.x</version><!-- 以最新发布版本为准 -->
</dependency>
```
`Maven`库查看最新版本: [https://repo1.maven.org/maven2/top/lingkang/final-security-core](https://repo1.maven.org/maven2/top/lingkang/final-security-core)

配置类
```java
@Component
public class MyFinalConfig extends FinalSecurityConfiguration {

    @Override
    protected void config(FinalHttpProperties properties) {
        properties.checkAuthorize()
                .pathMatchers("/user").hasAnyRole("user", "vip1") // 有其中任意角色就能访问
                .pathMatchers("/vip/**").hasAllRole("user", "vip1") // 必须有所有角色才能访问
                .pathMatchers("/**").hasLogin();// 需要登录才能访问

        // 排除鉴权路径匹配, 匹配优先级别：排除路径 > checkAuthorize > 注解
        properties.setExcludePath("/login", "/logout", "/user/login/app", "/**.js", "/**.css");
    }
}
```
启动类
```java
@FinalCheck(anyRole = "admin")// 注解鉴权
@Controller
public class ExampleApp {
    public static void main(String[] args) {
        Solon.start(ExampleApp.class, new String[]{});
    }

    @Get
    @Mapping("/")
    public Object index(Context context) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("context", context);
        return new ModelAndView("template.ftl", map);
    }

    @Inject
    private FinalSecurityHolder securityHolder;

    @Mapping("/login")
    public Object login() {
        securityHolder.login("lk", new String[]{"admin"});
        return "ok";
    }
    
    @FinalCheck(anyRole = {"user"})// 注解鉴权
    @Mapping("/test")
    public Object test() {
        return "注解权限";
    }

    @Mapping("/a")
    public Object a() {
        return "a";
    }
}
```
提示：
* 默认添加了注解鉴权
* bean注入一spring类似

### 前端解析视图中获取用户、角色

final-security依赖session，直接从session中读取即可。
solon需要提前设置好视图数据：
```java
    @Get
    @Mapping("/")
    public Object index(Context context) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("context", context);
        return new ModelAndView("template.ftl", map);
    }
```
`建议继承 ModelAndView 进行请求上下文设置这个值`
```html
是否登陆： ${context.session("final_isLogin")?string('yes', 'no')}<#--输出布尔类型特殊处理-->
<br>
账户名: ${context.session("final_loginUsername")!}
<br>
角色：
<#list context.session("final_hasRoles") as role> <#--遍历数组-->
${role},
</#list>
```
![pay](https://gitee.com/lingkang_top/final-security/raw/master/document/fontend-solon.png)

其他参考：https://gitee.com/lingkang_top/final-security