package top.lingkang.finalsecurity.common.base;

/**
 * @author lingkang
 * Created by 2022/1/7
 * @since 1.0.0
 * 需要注意，
 * request、response 在 servlet 为 HttpServletRequest 、HttpServletResponse
 * request、response 在 solon 下均为 org.noear.solon.core.handle.Context
 */
public interface FinalExceptionHandler<Request,Response> {
    /**
     * 需要注意，
     * request、response 在 servlet 为 HttpServletRequest 、HttpServletResponse
     * request、response 在 solon 下均为 org.noear.solon.core.handle.Context
     */
    void permissionException(Throwable e, Request request, Response response);

    /**
     * 需要注意，
     * request、response 在 servlet 为 HttpServletRequest 、HttpServletResponse
     * request、response 在 solon 下均为 org.noear.solon.core.handle.Context
     */
    void notLoginException(Throwable e, Request request, Response response);

    /**
     * 需要注意，
     * request、response 在 servlet 为 HttpServletRequest 、HttpServletResponse
     * request、response 在 solon 下均为 org.noear.solon.core.handle.Context
     */
    void exception(Throwable e, Request request, Response response);
}
