package top.lingkang.finalsecurity.common.base;

/**
 * @author lingkang
 * Created by 2022/10/28
 * @since 3.0.0
 */
public interface FinalSessionObject {
    Object getAttribute(String name);
}
