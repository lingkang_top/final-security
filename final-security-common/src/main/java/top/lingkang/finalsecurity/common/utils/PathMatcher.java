package top.lingkang.finalsecurity.common.utils;

import java.util.Comparator;
import java.util.Map;

/**
 * 参考 spring 5.x 版本
 * @since 1.0.0
 */
public interface PathMatcher {
    boolean isPattern(String var1);

    boolean match(String var1, String var2);

    boolean matchStart(String var1, String var2);

    String extractPathWithinPattern(String var1, String var2);

    Map<String, String> extractUriTemplateVariables(String var1, String var2);

    Comparator<String> getPatternComparator(String var1);

    String combine(String var1, String var2);
}
