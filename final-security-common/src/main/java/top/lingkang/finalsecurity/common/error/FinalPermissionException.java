package top.lingkang.finalsecurity.common.error;

/**
 * @author lingkang
 * date 2022/1/8
 * @since 1.0.0
 */
public class FinalPermissionException extends FinalBaseException{
    public FinalPermissionException(String message) {
        super(message);
    }
}
