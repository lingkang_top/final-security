package top.lingkang.finalsecurity.common.error;

/**
 * @author lingkang
 * date 2022/1/8
 * @since 1.0.0
 */
public class FinalNotLoginException extends FinalBaseException{
    public FinalNotLoginException(String message) {
        super(message);
    }
}
